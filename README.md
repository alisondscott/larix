# Larix laricina foliage senescence

Trying this out. For now starting with read counts, picking up after Olivia's work with eXpress

## DESeq2 for pairwise differential expression

Running this locally in R 3.6.3! Largely following the tutorial from [UCONN CBC](https://github.com/CBC-UCONN/RNA-seq-with-reference-genome-and-annotation#7-pairwise-differential-expression-with-counts-in-r-using-deseq2).

loading libraries and setting some basics:

        #trying out DESeq2
        library(DESeq2)
        library(tidyverse)

        directory <- "."

        sampleFiles <- list.files(directory, pattern = "*xprs.txt.sorted", full.names = TRUE)

For the full larch dataset, we're working with 18 input files. There are six trees (K1, K2, K3, U1, U2, U3) from two locations (U = UConn, K = Killingworth). Each of the six trees was sampled at three timepoints (T1, T2, T3). The name of the accessions are alphanumeric, with the first character indicating origin, second character the tree ID, and third number the timepoint. So sample **K13** is timepoint **3** from Tree **1** in **K**illingworth. 

Next we'll set the sample names, origins, and timepoint for each input file by creating a vector - making sure to keep things in the same order as the sampleFiles list. 


        sampleNames = c("K11", "K12", "K13", "K21", "K22", "K23", "K31", "K32", "K33", "U11", "U12", "U13", "U21", "U22", "U23", "U31", "U32", "U33")
        #Since we had more than one "condition" I listed both origin and timepoint descriptors
        sampleOrigin = c("Killingworth", "Killingworth", "Killingworth", "Killingworth", "Killingworth", "Killingworth", "Killingworth", "Killingworth", "Killingworth", "UConn", "UConn", "UConn", "UConn", "UConn", "UConn", "UConn", "UConn", "UConn")
        sampleTimepoint = c("T1", "T2", "T3", "T1", "T2", "T3", "T1", "T2", "T3", "T1", "T2", "T3", "T1", "T2", "T3", "T1", "T2", "T3")


        sampleTable <- data.frame(
        sampleName = sampleNames,
        fileName = sampleFiles,
        origin = sampleOrigin,
        timepoint = sampleTimepoint
        )

        ddsHTSeq <- DESeqDataSetFromHTSeqCount(
        sampleTable = sampleTable, 
        directory = directory, 
        design = ~ timepoint
        )


        # what does expression look like across genes?

        # sum counts for each gene across samples
        sumcounts <- rowSums(counts(ddsHTSeq))
        # take the log
        logsumcounts <- log(sumcounts,base=10)
        # plot a histogram of the log scaled counts
        hist(logsumcounts,breaks=100)




Plots can be [viewed here](https://docs.google.com/spreadsheets/d/1aNu-yxGUMohvOLz2hrveFE9wTEf2e-2WY9JyP0itmRI/edit#gid=0)


        # you can see the typically high dynamic range of RNA-Seq, with a mode in the distribution around 1000 fragments per gene, but some genes up over 1 million fragments. 

        # get genes with summed counts greater than 20
        keep <- sumcounts > 10

        # keep only the genes for which the vector "keep" is TRUE
        ddsHTSeq <- ddsHTSeq[keep,]


                # get results table
                res <- results(dds)


#### T1 vs T2 
                #test of results
                #here I'm using contrast to identify the timepoints I want to see comparisons between.
                #unclear if this is the right thing to do, but some online tutorials suggest maybe it is?
                res.t1t2 = results(dds, contrast=c("timepoint","T1","T2"))

                #continuing with T1 vs T2
                #shrink them i guess
                res_shrink_1v2 <- lfcShrink(dds, contrast=c("timepoint","T1","T2"))
                res_shrink_1v2[order(-abs(res_shrink_1v2$log2FoldChange)),][1:20,]
                plotMA(res_shrink_1v2, ylim=c(-4,4))

see the MA plot [here](https://docs.google.com/spreadsheets/d/1aNu-yxGUMohvOLz2hrveFE9wTEf2e-2WY9JyP0itmRI/edit?usp=sharing)

                # negative log-scaled adjusted p-values
                log_padj_1v2 <- -log(res_shrink_1v2$padj,10)
                log_padj_1v2[log_padj_1v2 > 100] <- 100

                # plot
                volcano_1v2 <- plot(x=res_shrink_1v2$log2FoldChange,
                y=log_padj_1v2,
                pch=20,
                cex=.2,
                col=(log_padj_1v2 > 10)+1, # color padj < 0.1 red
                ylab="negative log-scaled adjusted p-value",
                xlab="shrunken log2 fold changes")


see the volcano plot [here](https://docs.google.com/spreadsheets/d/1aNu-yxGUMohvOLz2hrveFE9wTEf2e-2WY9JyP0itmRI/edit?usp=sharing)

Also making some volcano plots with [EnhancedVolcano](https://bioconductor.org/packages/release/bioc/vignettes/EnhancedVolcano/inst/doc/EnhancedVolcano.html).

        #EnhancedVolcano!

        EnhancedVolcano(res_shrink_1v2,
                        lab = rownames(res_shrink_1v2),
                        x = 'log2FoldChange',
                        y = 'pvalue',
                        xlim = c(-8, 8),
                        title = 'T1 vs T2',
                        pCutoff = 10e-6,
                        FCcutoff = 1.5,
                        pointSize = 0.5,
                        labSize = 3.0)

see the enhanced volcano plot [here](https://docs.google.com/spreadsheets/d/1aNu-yxGUMohvOLz2hrveFE9wTEf2e-2WY9JyP0itmRI/edit?usp=sharing)


#### T2 vs T3
To do the comparison between timepoints 2 & 3, we need to set the reference condition to T2 using "relevel"

                #relevel to make T2 default
                ddsHTSeq$timepoint <- relevel(ddsHTSeq$timepoint, ref = "T2")

                dds_relevel <- DESeq(ddsHTSeq)

                # get results table
                res_relevel <- results(dds_relevel)

                #shrink them i guess
                res_shrink_relevel <- lfcShrink(dds_relevel, contrast=c("timepoint","T2","T3"))
                res_shrink_relevel[order(-abs(res_shrink_relevel$log2FoldChange)),][1:20,]
                plotMA(res_shrink_relevel, ylim=c(-4,4))

                # negative log-scaled adjusted p-values
                log_padj_relevel <- -log(res_shrink_relevel$padj,10)
                log_padj_relevel[log_padj_relevel > 100] <- 100

                # plot
                volcano_relevel <- plot(x=res_shrink_relevel$log2FoldChange,
                                y=log_padj_relevel,
                                pch=20,
                                cex=.2,
                                col=(log_padj_relevel > 10)+1, # color padj < 0.1 red
                                ylab="negative log-scaled adjusted p-value",
                                xlab="shrunken log2 fold changes")


                #EnhancedVolcano!

                EnhancedVolcano(res_shrink_relevel,
                                lab = rownames(res_shrink_relevel),
                                x = 'log2FoldChange',
                                y = 'pvalue',
                                xlim = c(-8, 8),
                                title = 'T2 vs T3',
                                pCutoff = 10e-6,
                                FCcutoff = 1.5,
                                pointSize = 0.5,
                                labSize = 3.0)
See the MA plot, volcano plot, and enhanced volcano plot [here](https://docs.google.com/spreadsheets/d/1aNu-yxGUMohvOLz2hrveFE9wTEf2e-2WY9JyP0itmRI/edit#gid=0).

